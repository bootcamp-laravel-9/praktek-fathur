<?php

namespace Database\Seeders;

use App\Models\TicketCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketCategory::create(
            [
                'name' => 'Student',
                'detail' => 'Special student price tag'
            ],
            [
                'name' => 'General',
                'detail' => 'For general purpose'
            ]
        );
    }
}
