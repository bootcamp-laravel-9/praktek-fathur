<?php

namespace Database\Seeders;

use App\Models\TicketHeader;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketHeader::create(
            [
                'no_ticket' => 'A-123',
                'name' => 'John Doe',
                'email' => 'john@example.com',
                'phone_number' => '123-456',
                'address' => 'San Fransisco',
                'date_ticket' => Carbon::parse('2020-01-02')
            ],
            [
                'no_ticket' => 'A-124',
                'name' => 'John May',
                'email' => 'johnmay@example.com',
                'phone_number' => '123-456',
                'address' => 'San Fransisco',
                'date_ticket' => Carbon::parse('2020-01-02')
            ]
        );
    }
}
