<?php

namespace Database\Seeders;

use App\Models\TicketDetail;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketDetail::create(
            [
                'ticket_header_id' => '1',
                'ticket_category_id' => '1',
                'ticket_total' => '2',
            ],
            [
                'ticket_header_id' => '2',
                'ticket_category_id' => '2',
                'ticket_total' => '1',
            ]
        );
    }
}
