@extends('Layout.main')
@section('menu-user', 'active')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between mb-2">
            <h3 class="h3 text-gray-800">Tables</h3>
            <a href="{{ url('user/add') }}" class="btn btn-primary">Add User</a>
        </div>
        @if (Session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
                {{ session('success') }}
            </div>
        @endif

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
            </div>
            <div class="card-body">
                <div>
                    <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="">
                                        <a href="{{ url('user/edit/' . $user->id) }}" class="btn btn-success"><i
                                                class="fas fa-user-edit"></i></a>
                                        <a href="{{ url('user/delete/' . $user->id) }}" class="btn btn-danger"><i
                                                class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
