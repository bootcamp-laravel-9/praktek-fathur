@extends('Layout.main')
@section('menu-user', 'active')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-flex justify-content-between mb-2">
            <h3 class="h3 text-gray-800">Edit user</h3>
        </div>

        <form class="user" method="post" action="/user/update">
            @csrf
            <div class="form-group">
                <div>
                    <input type="text" class="form-control form-control-user" id="name" name="name"
                        value="{{ $data->name }}" placeholder="Enter Your Name...">
                    @error('name')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div>
                    <input type="email" class="form-control form-control-user" id="email" name="email"
                        value="{{ $data->email }}" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    @error('email')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div>
                    <input type="password" class="form-control form-control-user" id="password" name="password"
                        placeholder="Password">
                    @error('password')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <input type="hidden" id="id" name="id" value="{{ $data->id }}">
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Submit
            </button>
        </form>
    </div>
@endsection
