@extends('Layout.main')
@section('menu-user', 'active')

@section('content')
    <div class="container-fluid">
        @if (Session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>
        @endif
        <!-- Page Heading -->
        <div class="d-flex justify-content-between mb-2">
            <h3 class="h3 text-gray-800">Add new user</h3>
        </div>

        <form class="user" method="post" action="store">
            @csrf
            <div class="form-group">
                <div>
                    <input type="text" class="form-control form-control-user" id="name" name="name"
                        value="{{ old('name') }}" placeholder="Enter Your Name...">
                    @error('name')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div>
                    <input type="email" class="form-control form-control-user" id="email" name="email"
                        value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    @error('email')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div>
                    <input type="password" class="form-control form-control-user" id="password" name="password"
                        placeholder="Password">
                    @error('password')
                        <p class="text-danger small">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Submit
            </button>
        </form>
    </div>
@endsection
