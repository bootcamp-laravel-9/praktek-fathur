@extends('Layout.main')
@section('menu-ticket', 'active')

@section('content')
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-flex justify-content-between mb-2">
            <h3 class="h3 text-gray-800">Ticket Report</h3>
            <a href="{{ url('ticket/add') }}" class="btn btn-primary">Buy Ticket</a>
        </div>


        <table id="example" class="table table-striped table-bordered" style="width:100%">
            <div class="row mb-3">
                <div class="col-md-3">
                    <label for="search" class="form-label">Search</label>
                    <input type="text" id="search" class="form-control" placeholder="Search...">
                </div>
                <div class="col-md-3">
                    <label for="category-filter" class="form-label">Category</label>
                    <select id="category-filter" class="form-select">
                        <option value="">All</option>
                        @foreach ($category as $data)
                            <option value="{{ $data->name }}">{{ $data->name }}</option>
                        @endforeach
                        <!-- Add more options as needed -->
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="date-range-filter" class="form-label">Date Range</label>
                    <div class="input-group" id="date-range-filter">
                        <input type="date" id="start-date" class="form-control">
                        <span class="input-group-text">to</span>
                        <input type="date" id="end-date" class="form-control">
                    </div>
                </div>
            </div>
            <thead>
                <tr>
                    <th>Ticket Id</th>
                    <th>Name</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($combinedData as $ticket)
                    @foreach ($category as $data)
                        @if ($data->id == $ticket['ticket_category_id'])
                            <tr>
                                <th>{{ $ticket['no_ticket'] }}</th>
                                <th>{{ $ticket['name'] }}</th>
                                <th>{{ $ticket['date_ticket'] }}</th>
                                <th>{{ $data->name }}</th>
                                <th>{{ $ticket['ticket_total'] }}</th>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script>
        // $("#search")
        //     .on("keyup", function() {
        //         var value = $(this).val();
        //         console.log(value);
        //     })
        //     .trigger("keyup");
        var table = $('#example').DataTable({
            info: false,
            ordering: false,
            // paging: false,
            "dom": '<"top"l>rt<"bottom"ip>'
        });

        $('#search').on('keyup', function() {
            table.search($(this).val()).draw();
        });

        // Add event listener for category filter
        $('#category-filter').on('change', function() {
            var category = $(this).val();
            table.columns(3).search(category).draw();
        });

        // Add event listener for date range filter
        $('#date-range-filter').on('change', function() {
            var startDate = $('#start-date').val();
            var endDate = $('#end-date').val();
            startDate = startDate ? startDate : '';
            endDate = endDate ? endDate : '';
            table.columns(2).search(startDate + '|' + endDate, true, false).draw();
        });
    </script>
@endsection
