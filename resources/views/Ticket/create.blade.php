@extends('Layout.main')
@section('menu-ticket', 'active')

@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb-2">
            <h3 class="h3 text-gray-800">Buy Ticket</h3>
        </div>
        @if (session('errorMsg'))
            @foreach (session('errorMsg') as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert">
                    {{ $error[0] }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endforeach
        @endif
        <form class="user" method="post" action="store">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control form-control-user" id="name" name="name"
                    value="{{ old('name') }}" placeholder="Enter Your Name...">
            </div>
            <div class="form-group">
                <input type="email" class="form-control form-control-user" id="email" name="email"
                    value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter Email Address...">
            </div>
            <div class="form-group">
                <input type="text" class="form-control form-control-user" id="phone_number" name="phone_number"
                    value="{{ old('phone_number') }}" placeholder="+62-934-716-223">
            </div>
            <div class="form-group">
                <input type="text" class="form-control form-control-user" id="address" name="address"
                    value="{{ old('address') }}" placeholder="Enter House Address...">
            </div>
            <div class="form-group">
                <input type="date" class="form-control form-control-user min-today" id="date_ticket" name="date_ticket"
                    value="{{ old('date_ticket') }}">
            </div>
            <div class="form-group">
                <input type="number" class="form-control form-control-user" id="ticket_total" name="ticket_total"
                    value="{{ old('ticket_total') }}" placeholder="Enter Number of Ticket..."">
            </div>
            <div class="form-group">
                <select id="cars" name="ticket_category_id" class="form-select form-control-user"
                    value="{{ old('ticket_category_id') }}">
                    @foreach ($category as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-select form-control-user">
                    <option value="qris">QRIS</option>
                    <option value="transfer">Transfer</option>
                    <option value="cod">COD</option>
                </select>
            </div>
            {{-- <input type="hidden" name="ticket_category_id" value="1"> --}}
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Submit
            </button>
        </form>
    </div>
@endsection
