<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Fathurrohman</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @yield('menu-dashboard')">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item @yield('menu-user')">
        <a class="nav-link" href="/user">
            <i class="fa-solid fa-user"></i>
            <span>User</span></a>
    </li>
    <li class="nav-item @yield('menu-ticket')">
        <a class="nav-link" href="/ticket">
            <i class="fa-solid fa-ticket"></i>
            <span>Ticket</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/logout">
            <i class="fa-solid fa-right-from-bracket"></i>
            <span>Logout</span></a>
    </li>

</ul>
<!-- End of Sidebar -->
