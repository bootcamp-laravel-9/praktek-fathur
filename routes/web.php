<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::resource('user', 'UserController');
Route::group(['middleware' => ['isNotAuth']], function () use ($router) {
    Route::get('login', 'ConsumeApiAuthController@index')->name('login');
    Route::post('login', 'ConsumeApiAuthController@login');
});

// Route::middleware('api.key')->get('brand', 'BrandController@index');

Route::group(['middleware' => ['isAuth']], function () use ($router) {
    Route::get('/', function () {
        return view('Dashboard.index');
    });

    Route::get('user', 'UserController@index');
    Route::get('user/add', 'UserController@create');
    Route::post('user/store', 'UserController@store');
    Route::get('user/edit/{id}', 'UserController@edit');
    Route::post('user/update', 'UserController@update');
    Route::get('user/delete/{id}', 'UserController@delete');

    Route::get('ticket', 'ConsumeApiTicketController@index');
    Route::get('ticket/add', 'ConsumeApiTicketController@create');
    Route::post('ticket/store', 'ConsumeApiTicketController@store');

    Route::get('logout', 'ConsumeApiAuthController@logout');
});
