<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['auth:sanctum', 'api.key']], function () use ($router) {
    // Route::get('user', 'UserController@index');
    // Route::post('user', 'UserController@store');
    // Route::put('user', 'UserController@update');
    // Route::delete('user/{id}', 'UserController@delete');

    Route::get('ticket-header', 'TicketHeaderController@index');
    Route::post('ticket-header', 'TicketHeaderController@store');

    Route::get('ticket-detail', 'TicketDetailController@index');
    Route::post('ticket-detail', 'TicketDetailController@store');
});
