<?php

namespace App\Repositories;

use App\Http\Resources\TicketDetailResource;
use App\Http\Resources\TicketHeaderResource;
use App\Http\Resources\TicketResource;
use App\Models\TicketDetail;
use App\Models\TicketHeader;
use Illuminate\Support\Facades\Auth;

class TicketDetailRepository
{
    protected $ticketDetail;

    public function __construct(TicketDetail $ticketDetail)
    {
        $this->ticketDetail = $ticketDetail;
    }

    public function all($id = null)
    {
        $response = TicketDetailResource::collection($this->ticketDetail->all());
        return $response;
    }


    public function storeOrUpdate($request)
    {
        $data = $this->ticketDetail->updateOrCreate([
            'id' => $request->id ?? null
        ], [
            'ticket_header_id' => $request->ticket_header_id,
            'ticket_category_id' => $request->ticket_category_id,
            'ticket_total' => $request->ticket_total,
        ]);
        return $data;
    }
}
