<?php

namespace App\Repositories;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'login failed, please check your credentials'
            ], 400);
        }

        $user = User::where('email', $credentials['email'])->first();

        $user['token'] = $user->createToken(config('app.name'))->plainTextToken;

        return response()->json([
            'status' => true,
            'message' => 'login success',
            'data' => $user
        ], 200);
    }

    public function all($id = null)
    {
        if ($id == null) {
            $response = UserResource::collection($this->user->all());
            return $response;
        }

        //get by id
        $data = $this->user->find($id);
        if (!$data) {
            throw new \Exception("data kendaraan tidak ditemukan.", 400);
        }

        $response = new UserResource($data);
        return $response;
    }


    public function storeOrUpdate($request, $method = null)
    {
        if ($method == 'PUT' && !$this->user->find($request->id)) {
            throw new \Exception('Data user tidak ditemukan', 400);
        }

        $data = $this->user->updateOrCreate([
            'id' => $request->id ?? null
        ], [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return $data;
    }

    public function destroy($id)
    {
        $data = $this->user->find($id);

        if (!$data) {
            throw new \Exception("data user tidak ditemukan.", 400);
        }

        $data->delete();
    }
}
