<?php

namespace App\Repositories;

use App\Http\Resources\TicketHeaderResource;
use App\Http\Resources\TicketResource;
use App\Models\TicketDetail;
use App\Models\TicketHeader;
use Illuminate\Support\Facades\Auth;

class TicketHeaderRepository
{
    protected $ticketHeader;

    public function __construct(TicketHeader $ticketHeader)
    {
        $this->ticketHeader = $ticketHeader;
    }

    public function all()
    {
        $response = TicketHeaderResource::collection($this->ticketHeader->all());
        return $response;
    }


    public function storeOrUpdate($request)
    {
        $data = $this->ticketHeader->updateOrCreate([
            'id' => $request->id ?? null
        ], [
            'no_ticket' => $request->no_ticket,
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'date_ticket' => $request->date_ticket
        ]);
        return $data;
    }
}
