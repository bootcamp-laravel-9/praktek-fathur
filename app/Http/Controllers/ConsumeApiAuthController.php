<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ConsumeApiAuthController extends Controller
{
    public function index()
    {
        return view('auth.index');
    }

    public function login(Request $request)
    {
        $request = Request::create(url("http://localhost:8000/api/login"), 'POST');
        $request->headers->set('accept', 'application/json');
        $request->headers->set('X-API-KEY', 'Bootcamp_9');
        $response = app()->make('router')->dispatch($request);

        if ($response->getStatusCode() == 200) {
            session(['sessionLogin' => $response->original["token"]]);
            return redirect('/');
        }
        $errorMsg = json_decode($response->getContent(), true);
        $message = $errorMsg['message'];
        return redirect('login')->with('loginMessage', $message);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
