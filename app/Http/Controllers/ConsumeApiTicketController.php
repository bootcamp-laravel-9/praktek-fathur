<?php

namespace App\Http\Controllers;

use App\Models\TicketCategory;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ConsumeApiTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $client = new Client();
        // $response = $client->get('http://127.0.0.1:8000/api/ticket-header');
        // $data = json_decode($response->getBody());
        // dd(session('sessionLogin'));
        $requestHeader = Request::create(url("http://localhost:8000/api/ticket-header"), 'GET');
        $requestHeader->headers->set('Content-Type', 'application/json');
        $requestHeader->headers->set('X-API-KEY', 'Bootcamp_9');
        $requestHeader->headers->set('Authorization', 'Bearer ' . session('sessionLogin'));
        $responseHeader = app()->make('router')->dispatch($requestHeader);
        $header = json_decode($responseHeader->getContent(), true);

        $requestDetail = Request::create(url("http://localhost:8000/api/ticket-detail"), 'GET');
        $requestDetail->headers->set('Content-Type', 'application/json');
        $requestDetail->headers->set('X-API-KEY', 'Bootcamp_9');
        $requestDetail->headers->set('Authorization', 'Bearer ' . session('sessionLogin'));
        $responseDetail = app()->make('router')->dispatch($requestDetail);
        $detail = json_decode($responseDetail->getContent(), true);

        $combinedData = [];

        // Iterate over the data from the first JSON
        foreach ($detail['data'] as $item1) {
            // Iterate over the data from the second JSON
            foreach ($header['data'] as $item2) {
                // Check if the fields match for combining
                if ($item1['ticket_header_id'] === $item2['id']) {
                    // Merge the items into a single array
                    $combinedItem = array_merge($item1, $item2);
                    // Add the combined item to the combined data array
                    $combinedData[] = $combinedItem;
                    // Break the inner loop as we found a match
                    break;
                }
            }
        }

        $category = TicketCategory::all();

        return view('Ticket.index', compact('combinedData', 'category'));
        dd($combinedData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = TicketCategory::all();
        return view('Ticket.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(["no_ticket" => Str::uuid()->toString()]);
        $ticketHeader = $request->except(['ticket_category_id', 'ticket_total']);
        $ticketHeader = Request::create(url("http://localhost:8000/api/ticket-header"), 'POST');
        $ticketHeader->headers->set('Accept', 'application/json');
        $ticketHeader->headers->set('X-API-KEY', 'Bootcamp_9');
        $responseHeader = app()->make('router')->dispatch($ticketHeader);

        if ($responseHeader->getStatusCode() == 200) {
            $request->merge(["ticket_header_id" => $responseHeader->original['data']['id']]);
            $ticketDetail = $request->only(['ticket_category_id', 'ticket_total', 'ticket_header_id']);
            $ticketDetail = Request::create(url("http://localhost:8000/api/ticket-detail"), 'POST');
            $ticketDetail->headers->set('Accept', 'application/json');
            $ticketDetail->headers->set('X-API-KEY', 'Bootcamp_9');
            $responseDetail = app()->make('router')->dispatch($ticketDetail);

            if ($responseDetail->getStatusCode() == 200) {
                return redirect('/ticket');
            }
            return back()->withInput();
        }
        $errorMsg = json_decode($responseHeader->getContent(), true);
        return back()->withInput()->with('errorMsg', $errorMsg['errors']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
