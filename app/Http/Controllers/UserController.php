<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $UserRepository;

    public function __construct(UserRepository $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $data = $request->id === null
        //     ? $this->UserRepository->all()
        //     : $data = $this->UserRepository->all($request->id);
        // return $data;
        $data = User::all();
        return view('user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|regex:/^[^\W\d_]+$/',
            'email' => 'required|max:100|email|unique:users',
            'password' => 'required|min:8',
        ]);

        $validateData['password'] = bcrypt($request->password);

        $user = User::create($validateData);
        return redirect('user/add')->with('success', 'New user added successfully');
        // try {
        //     $data = $this->UserRepository->storeOrUpdate($request);
        //     return response()->json([
        //         'status' => true,
        //         'message' => 'user created successfully.',
        //         'data' => $data,
        //     ], 200);
        // } catch (\Exception $e) {
        //     return response()->json([
        //         'status' => false,
        //         'message' => $e->getMessage(),
        //     ], 400);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // try {
        //     $data = $this->UserRepository->storeOrUpdate($request, 'PUT');
        //     return response()->json([
        //         'status' => true,
        //         'message' => 'user updated successfully.',
        //         'data' => $data,
        //     ], 200);
        // } catch (\Exception $e) {
        //     return response()->json([
        //         'status' => false,
        //         'message' => $e->getMessage(),
        //     ], 400);
        // }
        // dd($request->new_password);
        $updateprofile = User::find($request->id);
        $request->validate(['name' => 'required|regex:/^[a-zA-Z\s]+$/']);

        if ($updateprofile->email !== $request->email) {
            $request->validate(['email' => 'required|max:100|email|unique:users']);
        }

        if ($request->password != null) {
            $request->validate(['password' => 'required']);
        }

        $updateprofile->id = $request->id;
        $updateprofile->name = $request->name;
        $updateprofile->email = $request->email;
        if ($request->password != null) {
            $updateprofile->password = bcrypt($request->password);
        }

        if ($updateprofile->save()) {
            return redirect('/user')->with('success', 'data user updated successfuly');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // try {
        //     $data = $this->UserRepository->destroy($id);
        //     return response()->json([
        //         'status' => true,
        //         'message' => 'delete user successfully.'
        //     ], 200);
        // } catch (\Exception $e) {
        //     return response()->json([
        //         'status' => false,
        //         'message' => $e->getMessage(),
        //     ], 400);
        // }
        $profileData = User::find($id);
        $profileData->delete();
        return redirect('/user')->with('success', 'data user berhasil dihapus');
    }
}
